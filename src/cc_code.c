#include "cc_code.h"

int rCount = 0;
int lCount = 0;

///////////////////////////////////////////////////////////////////////////////////////////
comp_instruction_t * codeVetor(comp_tree_t * node, comp_code_param_t params);
void addOperation(comp_instruction_t * instruction, int type, char * label, char * sourceA, char * sourceB, char * target);
void concatCode(comp_instruction_t * instA, comp_instruction_t * instB);
comp_instruction_t * createInstruction();
char * generateLabel(int type);
comp_instruction_t * translate(comp_tree_t * node, comp_code_param_t params);
//////////////////////////////////////////////////////////////////////////////////////////

comp_instruction_t * codeAritmetica(comp_tree_t *node, comp_code_param_t params)
{
	char * ra = NULL;
	char * rb = NULL;
	char * rr = NULL;
	
	comp_instruction_t *instruction = NULL;
	comp_instruction_t *valueA = NULL;
	comp_instruction_t *valueB = NULL;
	comp_instruction_t *next = NULL;
	
	int command = -1;
	
	//printf("TODO Aritmetica\n");
	
	ra = generateLabel(TYPE_REGISTER);
	rb = generateLabel(TYPE_REGISTER);
	
	rr = params.rr;
	
	params.rr = ra;
	valueA = translate(node->child, params);
	
	params.rr = rb;
	valueB = translate(node->child->brother, params);
	
	next = translate(node->child->brother->brother, params);
	
	instruction = createInstruction();
	
	switch(node->type)
	{
		case AST_ARIM_SOMA:
			command = OPCODE_ADD;
			break;	
		case AST_ARIM_SUBTRACAO:
			command = OPCODE_SUB;
			break;
		case AST_ARIM_MULTIPLICACAO:
			command = OPCODE_MULT;
			break;
		case AST_ARIM_DIVISAO:
			command = OPCODE_DIV;
			break;
		case AST_SHIFT_RIGHT:
			command = OPCODE_RSHIFT;
			break;		
		case AST_SHIFT_LEFT:
			command = OPCODE_LSHIFT;
			break;
	}
	
	addOperation(instruction, command, NULL, ra, rb, rr);
	
	concatCode(valueA, valueB);
	concatCode(valueB, instruction);
	concatCode(instruction, next);
	
	free(ra);
	free(rb);

	return valueA;
	
}

comp_instruction_t * codeAtribuicao(comp_tree_t *node, comp_code_param_t params)
{
	char *address = NULL;
	char *ra = NULL;
	char *rb = NULL;
	char *rc = NULL;
	char *sizeC = NULL;

	int size = 0;

	comp_tree_t * child;

	comp_instruction_t *instruction = NULL;
	comp_instruction_t *value = NULL;
	comp_instruction_t *next = NULL;
	comp_instruction_t *vetor = NULL;

	//printf("TODO Atribuição\n");
	
	ra = generateLabel(TYPE_REGISTER);

	params.rr = ra;

	value = translate(node->child->brother, params);
	next = translate(node->child->brother->brother, params);

	instruction = createInstruction();

	if(node->child->type == AST_VETOR_INDEXADO)
	{
		rb = generateLabel(TYPE_REGISTER);

		params.rr = rb;

		vetor = codeVetor(node->child, params);

		if(node->child->child->global)
		{
			addOperation(instruction, OPCODE_STOREAO, NULL, "rbss", rb, ra);
		}
		else
		{
			addOperation(instruction, OPCODE_STOREAO, NULL, "rarp", rb, ra);
		}

		concatCode(value, vetor);
		concatCode(vetor, instruction);
	}
	else
	{

		asprintf(&address, "%d", node->child->address);

		if(node->child->global)
		{
			addOperation(instruction, OPCODE_STOREAI, NULL, "rbss", address, ra);
		}
		else
		{
			addOperation(instruction, OPCODE_STOREAI, NULL, "rarp", address, ra);
		}

		concatCode(value, instruction);

		free(address);
	}

	concatCode(instruction, next);

	free(ra);
	free(rb);

	return value;
}

comp_instruction_t * codeBooleano(comp_tree_t * node, comp_code_param_t params)
{
	char *ra;

	comp_instruction_t * instruction = NULL;
	comp_instruction_t * expression = NULL;

	switch(node->type)
	{
		case AST_LOGICO_COMP_DIF:
		case AST_LOGICO_COMP_IGUAL:
		case AST_LOGICO_COMP_LE:
		case AST_LOGICO_COMP_GE:
		case AST_LOGICO_COMP_L:
		case AST_LOGICO_COMP_G:

			return translate(node, params);

			break;
		default:

			ra = generateLabel(TYPE_REGISTER);

			params.rr = ra;

			expression = translate(node, params);

			instruction = createInstruction();

			if(node->returnType != ISK_BOOL)
			{
				addOperation(instruction, OPCODE_CMP_NE, NULL, ra, "0", ra);
			}

			addOperation(instruction, OPCODE_CBR, NULL, params.lt, params.lf, ra);

			concatCode(expression, instruction);

			free(ra);

			return expression;
	}
}

comp_instruction_t * codeComparacao(comp_tree_t * node, comp_code_param_t params)
{
	int type = -1;
	
	char *ra = NULL;
	char *rb = NULL;
	char *rc = NULL;
	
	comp_instruction_t *instruction = NULL;
	comp_instruction_t *valueA = NULL;
	comp_instruction_t *valueB = NULL;
	
	switch(node->type)
	{
		case AST_LOGICO_COMP_DIF:
			type = OPCODE_CMP_NE;
			break;
		case AST_LOGICO_COMP_IGUAL:
			type = OPCODE_CMP_EQ;
			break;
		case AST_LOGICO_COMP_LE:
			type = OPCODE_CMP_LE;
			break;
		case AST_LOGICO_COMP_GE:
			type = OPCODE_CMP_GE;
			break;
		case AST_LOGICO_COMP_L:
			type = OPCODE_CMP_LT;
			break;
		case AST_LOGICO_COMP_G:
			type = OPCODE_CMP_GT;
			break;
	}

	instruction = createInstruction();
	
	ra = generateLabel(TYPE_REGISTER);
	rb = generateLabel(TYPE_REGISTER);
	rc = generateLabel(TYPE_REGISTER);

	addOperation(instruction, type, NULL, ra, rb, rc);
	addOperation(instruction, OPCODE_CBR, NULL, params.lt, params.lf, rc);

	params.rr = ra;
	valueA = translate(node->child, params);

	params.rr = rb;
	valueB = translate(node->child->brother, params);

	concatCode(valueA, valueB);
	concatCode(valueB, instruction);

	free(ra);
	free(rb);
	free(rc);

	return valueA;
}

comp_instruction_t * codeCondicional(comp_tree_t *node, comp_code_param_t params)
{
	char *lt = NULL;
	char *lf = NULL;
	char * finally = NULL;

	comp_instruction_t * begin = NULL;
	comp_instruction_t * middle = NULL;
	comp_instruction_t * end = NULL;

	comp_instruction_t * test = NULL;

	comp_instruction_t * child1 = NULL;
	comp_instruction_t * child2 = NULL;
	comp_instruction_t * next = NULL;

	lt = generateLabel(TYPE_LABEL);
	lf = generateLabel(TYPE_LABEL);

	params.lt = lt;
	params.lf = lf;

	finally = generateLabel(TYPE_LABEL);

	test = codeBooleano(node->child, params);

	begin = createInstruction();

	addOperation(begin, OPCODE_NOP, lt, NULL, NULL, NULL);

	concatCode(test, begin);

	child1 = translate(node->child->brother, params);

	concatCode(begin, child1);

	if(node->type == AST_IF)
	{
		end = createInstruction();
		addOperation(end, OPCODE_NOP, lf, NULL, NULL, NULL);

		concatCode(child1, end);

		next = translate(node->child->brother->brother, params);
		concatCode(end, next);
	}
	else
	{
		middle = createInstruction();
		addOperation(middle, OPCODE_JUMPI, NULL, NULL, NULL, finally);
		addOperation(middle, OPCODE_NOP, lf, NULL, NULL, NULL);

		concatCode(child1, middle);

		child2 = translate(node->child->brother->brother, params);

		concatCode(middle, child2);

		end = createInstruction();
		addOperation(end, OPCODE_NOP, finally, NULL, NULL, NULL);

		concatCode(child2, end);

		next = translate(node->child->brother->brother->brother, params);
		concatCode(end, next);
	}

	free(lt);
	free(lf);
	free(finally);

	return test;
}

comp_instruction_t * codeCurtoCircuito(comp_tree_t * node, comp_code_param_t params)
{
	char * backup;

	comp_instruction_t * testA;
	comp_instruction_t * testB;

	//printf("TODO Curto Circuito\n");

	if(node->type == AST_LOGICO_E)
	{
		backup = params.lt;

		params.lt = generateLabel(TYPE_LABEL);
		testA = codeBooleano(node->child, params);

		addOperation(testA, OPCODE_NOP, params.lt, NULL, NULL, NULL);
		free(params.lt);

		params.lt = backup;
		testB = codeBooleano(node->child->brother, params);
	}
	else
	{
		backup = params.lf;

		params.lf = generateLabel(TYPE_LABEL);
		testA = codeBooleano(node->child, params);

		addOperation(testA, OPCODE_NOP, params.lf, NULL, NULL, NULL);
		free(params.lf);

		params.lf = backup;
		testB = codeBooleano(node->child->brother, params);
	}

	concatCode(testA, testB);

	return testA;
}

comp_instruction_t * codeDeslocamento(comp_tree_t * node, comp_code_param_t params)
{
	int type = -1;
	
	char *address = NULL;
	char *ra = NULL;
	char *rb = NULL;
	char *rc = NULL;

	comp_instruction_t *instruction = NULL;
	comp_instruction_t *valueA = NULL;
	comp_instruction_t *valueB = NULL;
	comp_instruction_t *next = NULL;

	//printf("TODO deslocamento\n");

	switch(node->type)
	{
		case AST_SHIFT_LEFT:
			type = OPCODE_LSHIFT;
			break;
		case AST_SHIFT_RIGHT:
			type = OPCODE_RSHIFT;
			break;
	}

	instruction = createInstruction();
	
	ra = generateLabel(TYPE_REGISTER);
	rb = generateLabel(TYPE_REGISTER);
	rc = generateLabel(TYPE_REGISTER); 

	addOperation(instruction, type, NULL, ra, rb, rc);

	asprintf(&address, "%d", node->child->address);

	if(node->child->global)
	{
		addOperation(instruction, OPCODE_STOREAI, NULL, "rbss", address, rc);
	}
	else
	{
		addOperation(instruction, OPCODE_STOREAI, NULL, "rarp", address, rc);
	}

	params.rr = ra;
	valueA = translate(node->child, params);

	params.rr = rb;
	valueB = translate(node->child->brother, params);

	next = translate(node->child->brother->brother, params);

	concatCode(valueA, valueB);
	concatCode(valueB, instruction);
	concatCode(instruction, next);

	free(ra);
	free(rb);
	free(rc);
	free(address);

	return valueA;
}

comp_instruction_t * codeEnquanto(comp_tree_t * node, comp_code_param_t params)
{
	char * repeat = NULL;

	comp_instruction_t * head = NULL;
	comp_instruction_t * test = NULL;
	comp_instruction_t * code = NULL;
	comp_instruction_t * middle = NULL;
	comp_instruction_t * end = NULL;

	params.lf = generateLabel(TYPE_LABEL);
	params.lt = generateLabel(TYPE_LABEL);
	repeat = generateLabel(TYPE_LABEL);

	head = createInstruction();
	addOperation(head, OPCODE_NOP, repeat, NULL , NULL, NULL);

	test = codeBooleano(node->child, params);

	middle = createInstruction();

	concatCode(head, test);

	addOperation(middle, OPCODE_NOP, params.lt, NULL, NULL, NULL);

	concatCode(test, middle);

	code = translate(node->child->brother, params);

	concatCode(middle, code);

	end = createInstruction();

	addOperation(end, OPCODE_JUMPI, NULL, NULL, NULL, repeat);
	addOperation(end, OPCODE_NOP, params.lf, NULL, NULL, NULL);

	concatCode(code, end);

	free(params.lf);
	free(params.lt);
	free(repeat);

	return head;
}

comp_instruction_t * codeFacaEnquanto(comp_tree_t * node, comp_code_param_t params)
{
	comp_instruction_t * head = NULL;
	comp_instruction_t * begin = NULL;
	comp_instruction_t * end = NULL;
	comp_instruction_t * test = NULL;
	comp_instruction_t * code = NULL;

	params.lt = generateLabel(TYPE_LABEL);
	params.lf = generateLabel(TYPE_LABEL);

	head = createInstruction();
	addOperation(head, OPCODE_NOP, params.lt, NULL , NULL, NULL);

	code = translate(node->child, params);

	concatCode(head, code);

	test = translate(node->child->brother, params);

	concatCode(code, test);

	end = createInstruction();
	addOperation(end, OPCODE_NOP, params.lf, NULL, NULL, NULL);

	concatCode(test, end);

	free(params.lt);
	free(params.lf);

	return head;
}

comp_instruction_t * codeIdentificador(comp_tree_t *node, comp_code_param_t params)
{
	char * address;
	
	comp_instruction_t *instruction = NULL;
	comp_instruction_t *child = NULL;
	
	//printf("TODO Identificador\n");
	
	instruction = createInstruction();

	asprintf(&address, "%d", node->address);

	//printf("%s: %d \n", node->data->original, node->global);

	if(node->global)
	{
		addOperation(instruction, OPCODE_LOADAI, NULL, "rbss", address, params.rr);
	}
	else
	{
		addOperation(instruction, OPCODE_LOADAI, NULL, "rarp", address, params.rr);
	}	
	
	child = translate(node->child, params);
	
	concatCode(instruction, child);

	free(address);
	
	return instruction;
}

comp_instruction_t * codeLiteral(comp_tree_t *node, comp_code_param_t params)
{
	char * value;
	
	comp_instruction_t *instruction = NULL;
	comp_instruction_t *child = NULL;
	
	//printf("TODO Literal\n");

	instruction = createInstruction();
	
	switch(node->data->type)
	{
		case ISK_INT:
			asprintf(&value, "%d", node->data->value.integer);
			addOperation(instruction, OPCODE_LOADI, NULL, value, NULL, params.rr);
			free(value);
			break;
	}
	return instruction;
}

comp_instruction_t * codeVetor(comp_tree_t * node, comp_code_param_t params)
{
	char *maxI = NULL;
	char *rr = NULL;
	char *sizeS = NULL;

	char * address = 0;

	comp_instruction_t * instruction = NULL;
	comp_instruction_t * value = NULL;
	comp_instruction_t * first = NULL;
	comp_instruction_t * last = NULL;

	comp_tree_t * child;

	int iterator = 0;
	int sizeI = 0;

	switch(node->child->returnType)
	{
		case ISK_INT:
			sizeI = 4;
			break;
		case ISK_FLOAT:
			sizeI = 8;
			break;
		default:
			sizeI = 1;
			break;
	}

	asprintf(&sizeS, "%d", sizeI);
	asprintf(&address, "%d", node->child->address);

	if(node->child->params->count == 1)
	{
		first = translate(node->child->brother, params);

		instruction = createInstruction();

		addOperation(instruction, OPCODE_MULTI, NULL, params.rr, sizeS, params.rr);
		addOperation(instruction, OPCODE_ADDI, NULL, params.rr, address, params.rr);

		concatCode(first, instruction);
	}
	else
	{
		child = node->child->brother;

		value = translate(child, params);

		first = value;
		last = value;

		rr = params.rr;
		params.rr = generateLabel(TYPE_REGISTER);

		iterator = 1;

		while(iterator < node->child->params->count)
		{
			child = child->child;

			value = translate(child, params);

			concatCode(last, value);

			instruction = createInstruction();

			asprintf(&maxI, "%d", node->child->params->list[iterator]);

			addOperation(instruction, OPCODE_MULTI, NULL, rr, maxI, rr);
			addOperation(instruction, OPCODE_ADD, NULL, rr, params.rr, rr);

			concatCode(value, instruction);

			last = instruction;

			free(maxI);

			iterator++;
		}


		addOperation(instruction, OPCODE_MULTI, NULL, rr, sizeS, rr);
		addOperation(instruction, OPCODE_ADDI, NULL, rr, address, rr);

		free(params.rr);
	}

	free(address);

	return first;


}

//////////////////////////////////////////////////////////////////////////////////////////

void addOperation(comp_instruction_t * instruction, int type, char * label, char * sourceA, char * sourceB, char * target)
{
	comp_operation_t * pointer = NULL;
	
	//printf("Add param with type %d\n", type);

	pointer = (comp_operation_t *) malloc(sizeof(comp_operation_t));
	
	pointer->type = type;

	if(label != NULL)
	{
		asprintf(&pointer->label, "%s", label);
	}
	else
	{
		pointer->label = NULL;
	}
	
	if(sourceA != NULL)
	{
		asprintf(&pointer->sourceA, "%s", sourceA);
	}
	else
	{
		pointer->sourceA = NULL;
	}

	if(sourceB != NULL)
	{
		asprintf(&pointer->sourceB, "%s", sourceB);
	}
	else
	{
		pointer->sourceB = NULL;
	}

	if(target != NULL)
	{
		asprintf(&pointer->target, "%s", target);
	}
	else
	{
		pointer->target = NULL;
	}

	pointer->next = NULL;
		
	if(instruction->firstOp == NULL)
	{
		instruction->firstOp = pointer;

		pointer->back = NULL;
		
	}
	else
	{
		if(instruction->lastOp == NULL)
		{
			instruction->firstOp->next = pointer;
			pointer->back = instruction->firstOp; 
		}
		else
		{
			instruction->lastOp->next = pointer;
			pointer->back = instruction->lastOp;
		}		
		
		instruction->lastOp = pointer;
	}	
}

/**
 * Concatena duas listas de instruções
 * @param listA: Primeira lista
 * @param listB: Segunda lista
**/

void concatCode(comp_instruction_t * instA, comp_instruction_t * instB)
{
	comp_instruction_t * pointer;
	
	if(instB != NULL)
	{
		pointer = instA;
	
		while(pointer->next != NULL)
		{
			pointer = pointer->next;
		}
		
		pointer->next = instB;
		instB->back = pointer;
	}
}

/**
 * Cria uma instrução
**/

comp_instruction_t * createInstruction()
{
	comp_instruction_t * pointer = NULL;
	
	pointer = (comp_instruction_t *) malloc(sizeof(comp_instruction_t));
	
	pointer->back = NULL;
	pointer->next = NULL;
	pointer->firstOp = NULL;
	pointer->lastOp = NULL;
	
	return pointer;
}

/**
 * Gera um label baseado no tipo
 * @param type: tipo do label
 * @return: Retorna o rotulo dinamicamente alocado
**/

char * generateLabel(int type)
{
	char * label;
	
	switch(type)
	{
		case TYPE_REGISTER:
			asprintf(&label, "r%d", rCount);
			rCount++;
			break;
		case TYPE_LABEL:
			asprintf(&label, "L%d", lCount);
			lCount++;
			break;
	}
	
	return label;
}

/**
 * Função para imprimir uma operação
 * @param operação: Instrução a ser imprimida
 * @param label: Rotulo a ser impresso antes da operação, caso não exista deve ser NULL
**/

void outputOperation(comp_operation_t * operation)
{
	if(operation->label != NULL)
	{
		printf("%s: ", operation->label);
	}
	
	switch(operation->type)
	{
		case OPCODE_NOP:
			printf("nop\n");
			break;
		case OPCODE_ADD:
			printf("add %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_SUB:
			printf("sub %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_MULT:
			printf("mult %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_DIV:
			printf("div %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_ADDI:
			printf("addI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_SUBI:
			printf("subI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_RSUBI:
			printf("rsubI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_MULTI:
			printf("multI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_DIVI:
			printf("divI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_RDIVI:
			printf("rdivI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_LSHIFT:
			printf("lshift %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_LSHIFTI:
			printf("lshiftI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_RSHIFT:
			printf("rshift %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_RSHIFTI:
			printf("rshiftI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_AND:
			printf("add %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_ANDI:
			printf("andI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_OR:
			printf("or %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_ORI:
			printf("orI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_XOR:
			printf("xor %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_XORI:
			printf("xorI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_LOAD:
			printf("load %s => %s\n", operation->sourceA, operation->target);
			break;
		case OPCODE_LOADAI:
			printf("loadAI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_LOADAO:
			printf("loadAO %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_CLOAD:
			printf("cload %s => %s\n", operation->sourceA, operation->target);
			break;
		case OPCODE_CLOADAI:
			printf("cloadAI %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_CLOADAO:
			printf("cloadAO %s, %s => %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_LOADI:
			printf("loadI %s => %s\n", operation->sourceA, operation->target);
			break;
		case OPCODE_STORE:
			printf("store %s => %s\n", operation->target, operation->sourceA);
			break;
		case OPCODE_STOREAI:
			printf("storeAI %s => %s, %s\n", operation->target, operation->sourceA, operation->sourceB);
			break;
		case OPCODE_STOREAO:
			printf("storeAO %s => %s, %s\n", operation->target, operation->sourceA, operation->sourceB);
			break;
		case OPCODE_CSTORE:
			printf("cstore %s => %s\n", operation->target, operation->sourceA);
			break;
		case OPCODE_CSTOREAI:
			printf("cstoreAI %s => %s, %s\n", operation->target, operation->sourceA, operation->sourceB);
			break;
		case OPCODE_CSTOREAO:
			printf("cstoreAI %s => %s, %s\n", operation->target, operation->sourceA, operation->sourceB);
			break;
		case OPCODE_I2I:
			printf("i2i %s => %s\n", operation->sourceA, operation->target);
			break;
		case OPCODE_C2C:
			printf("c2c %s => %s\n", operation->sourceA, operation->target);
			break;
		case OPCODE_C2I:
			printf("c2i %s => %s\n", operation->sourceA, operation->target);
			break;
		case OPCODE_I2C:
			printf("i2c %s => %s\n", operation->sourceA, operation->target);
			break;
		case OPCODE_JUMPI:
			printf("jumpI -> %s\n", operation->target);
			break;
		case OPCODE_JUMP:
			printf("jump -> %s\n", operation->target);
			break;
		case OPCODE_CBR:
			printf("cbr %s -> %s, %s\n", operation->target, operation->sourceA, operation->sourceB);
			break;
		case OPCODE_CMP_LT:
			printf("cmp_LT %s, %s -> %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_CMP_LE:
			printf("cmp_LE %s, %s -> %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_CMP_EQ:
			printf("cmp_EQ %s, %s -> %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_CMP_GE:
			printf("cmp_GE %s, %s -> %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_CMP_GT:
			printf("cmp_GT %s, %s -> %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
		case OPCODE_CMP_NE:
			printf("cmp_NE %s, %s -> %s\n", operation->sourceA, operation->sourceB, operation->target);
			break;
	}
	
}

/**
 * Função recursiva para navegação da árvore
 * @params node: Ponto da arvore atual
**/

comp_instruction_t * translate(comp_tree_t * node, comp_code_param_t params)
{

	comp_instruction_t * list = NULL;
	
	if(node == NULL)
	{
		return NULL;
	}
	
	switch(node->type)
	{
		case AST_PROGRAMA:
			list = translate(node->child, params);
			break;
		case AST_FUNCAO:
			list = translate(node->child, params);;
			break;
		case AST_IF:
		case AST_IF_ELSE:
			list = codeCondicional(node, params);
			break;
		case AST_DO_WHILE:
			list = codeFacaEnquanto(node, params);
			break;
		case AST_WHILE_DO:
			list = codeEnquanto(node, params);
			break;
		case AST_INPUT:
			printf("TODO INPUT\n");
			break;
		case AST_OUTPUT:
			printf("TODO OUTPUT\n");
			break;
		case AST_ATRIBUICAO:
			list = codeAtribuicao(node, params);
			break;
		case AST_RETURN:
			printf("TODO RETURN\n");
			break;
		case AST_BLOCO:
			list = translate(node->child, params);
			break;
		case AST_IDENTIFICADOR:
			list = codeIdentificador(node, params);
			break;
		case AST_LITERAL:
			list = codeLiteral(node, params);
			break;
		case AST_ARIM_SOMA:
		case AST_ARIM_SUBTRACAO:
		case AST_ARIM_MULTIPLICACAO:
		case AST_ARIM_DIVISAO:
			list = codeAritmetica(node, params);
			break;
		case AST_SHIFT_RIGHT:
		case AST_SHIFT_LEFT:
			list = codeDeslocamento(node, params);
			break;
		case AST_ARIM_INVERSAO:
			printf("TODO INVERSÃO\n");
			break;
		case AST_LOGICO_E:
		case AST_LOGICO_OU:
			list = codeCurtoCircuito(node, params);
			break;
		case AST_LOGICO_COMP_DIF:
		case AST_LOGICO_COMP_IGUAL:
		case AST_LOGICO_COMP_LE:
		case AST_LOGICO_COMP_GE:
		case AST_LOGICO_COMP_L:
		case AST_LOGICO_COMP_G:
			list = codeComparacao(node, params);
			break;
		case AST_LOGICO_COMP_NEGACAO:
			printf("TODO NEGAÇÃO\n");
		case AST_VETOR_INDEXADO:
			printf("TODO VETOR\n");
			break;
		case AST_CHAMADA_DE_FUNCAO:
			printf("TODO CHAMDA FUNC\n");
			break;
	}
	
	return list;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void generateCode(comp_tree_t * tree)
{
	comp_instruction_t * lastInst = NULL;
	comp_instruction_t * list = NULL;
	
	comp_operation_t * pointer = NULL;
	comp_operation_t * lastOp = NULL;
	
	comp_code_param_t params;
	
	//printf("Generate Code\n");
	
	list = translate(tree, params); // Isola a chamada da função da implementação interna

	//printf("Print Code\n");
	
	while(list != NULL)
	{
		pointer = list->firstOp;

		while(pointer != NULL)
		{
			outputOperation(pointer);

			lastOp = pointer;
			pointer = pointer->next;

			if(lastOp->sourceA != NULL)
			{
				free(lastOp->sourceA);
			}

			if(lastOp->sourceB != NULL)
			{
				free(lastOp->sourceB);
			}

			if(lastOp->target != NULL)
			{
				free(lastOp->target);
			}

			if(lastOp->label != NULL)
			{
				free(lastOp->label);
			}

			free(lastOp);
		}

		lastInst = list;
		list = list->next;

		free(lastInst);
	}
}


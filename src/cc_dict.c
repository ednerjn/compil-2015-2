#include "cc_dict.h"
#include <stdio.h>

comp_dict_t symbol_hash;

comp_dict_item_t * addHash(comp_dict_t * table, char * item_key)
{
	ENTRY element, *pointer;
	comp_dict_item_t * item = NULL;
	
	item = (comp_dict_item_t *) malloc(sizeof(comp_dict_item_t));

	if(item != NULL)
	{
		element.key = item_key;
		element.data = (void *) item; 
	
		hsearch_r(element, ENTER, &pointer,table);

		return item;
	}
	else
	{
		return NULL;
	}	
}

comp_dict_item_t * findHash(comp_dict_t * table, char * item_key)
{
	ENTRY element, * item;
	
	element.key = item_key;

	hsearch_r(element, FIND, &item,table);
	
	if(item != NULL)
	{
		return item->data;
	}
	else
	{
		return NULL;
	}
}

int initHash(comp_dict_t * table)
{
	int status;
	
	memset(table, 0, sizeof(comp_dict_t));
	
	status = hcreate_r(MAX_HASH_TABLE,table);
	
	if(status != 0)
	{
		return 0;
	}
	else
	{
		return -1;
	}
}

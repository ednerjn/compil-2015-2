#include "cc_semantic.h"

int globalAddress = 0;
int lastFunction = -1;
int lastVariable = -1;

void addFunction(comp_dict_item_t* item, int type)
{
	//printf("add function: %s with type: %d\n", item->original, type);

	if(stackFind(item->original, 0) == NULL)
	{
		lastFunction = stackPush(item, type, 0, 0, true, true);
	}
	else
	{
		printf("%s is declared\n", item->original);
		exit(IKS_ERROR_DECLARED);
	}
}

void addParam(int type)
{
	comp_info_stack_t * element;

	element = stackGet(lastFunction);

	if(element == NULL)
	{
		printf("Function does not found\n");
		exit(-1);
	}
	else
	{
		if(element->params == NULL)
		{
			element->params = malloc(sizeof(comp_param_t));
			element->params->count = 0;
		}

		//printf("Add param to %s with type %d\n", element->data->original, type);
		element->params->list[element->params->count++] = type;
	}
}

void addVariable(comp_dict_item_t* item, int type, bool global)
{
	comp_info_stack_t * element;
	comp_info_stack_t * function;
	comp_scope_t * pointer;

	int size = 0;
	int address = 0;
	int position = 0;

	//printf("add variable: %s with type: %d\n", item->original, type);

	switch(type)
	{
		case ISK_INT:
			size = 4;
			break;
		case ISK_FLOAT:
			size = 8;
			break;
		case ISK_CHAR:
			size = 1;
			break;
		case ISK_STRING:
			size = strlen(item->original);
			break;
		case ISK_BOOL:
			size = 1;
	}

	if(global)
	{
		address = globalAddress;
		globalAddress+= size;
	}
	else
	{
		function = stackGet(lastFunction);

		if(function == NULL)
		{
			printf("Function does not found\n");
			exit(-1);
		}
		else
		{
			address = function->address;
			function->address += size;
		}
	}

	if(scopeStack == NULL)
	{
		element = stackFind(item->original, 0);
	}
	else
	{
		element = stackFind(item->original, scopeStack->index + 1);
	}

	if(element == NULL)
	{
		lastVariable = stackPush(item, type, size, address, false, global);
	}
	else
	{
		printf("%s is declared\n", item->original);
		exit(IKS_ERROR_DECLARED);
	}
}

void addVectorParam(int size)
{
	int oldSize = 0;
	int newSize = 0;

	comp_info_stack_t * element;
	comp_info_stack_t * function;

	element = stackGet(lastVariable);

	if(element == NULL)
	{
		printf("Vector does not found\n");
		exit(-1);
	}
	else
	{
		element->vector = true;

		if(element->params == NULL)
		{
			element->params = malloc(sizeof(comp_param_t));
			element->params->count = 0;
		}

		element->params->list[element->params->count++] = size;

		oldSize = element->size;
		newSize = oldSize * size;

		element->size = newSize;

		if(element->global)
		{
			globalAddress += newSize - oldSize;
		}
		else
		{
			function = stackGet(lastFunction);

			if(function == NULL)
			{
				printf("Function does not found\n");
				exit(-1);
			}
			else
			{
				function->address += newSize - oldSize;
			}
		}
	}
}

void checkParams(comp_tree_t * function, comp_tree_t * params)
{
	comp_info_stack_t * element;
	comp_tree_t * child;
	int count = 0;
	int type;

	if(function == NULL)
	{
		printf("checkParams: mission function\n");
		exit(-1);
	}

	element = stackFind(function->data->original, 0);

	if (element == NULL)
	{
		printf("%s is not declared\n", function->data->original);
		exit(IKS_ERROR_UNDECLARED);
	}
	else if(element->function == false)
	{
		printf("%s is not a function\n", function->data->original);
		exit(IKS_ERROR_VARIABLE);
	}
	else if(element->params == NULL && params == NULL)
	{
		return;
	}
	else if(element->params == NULL && params != NULL)
	{
		printf("Function %s has no params\n", function->data->original);
		exit(IKS_ERROR_EXCESS_ARGS);
	}
	else if(element->params != NULL && params == NULL)
	{
		printf("Function %s has params\n", function->data->original);
		exit(IKS_ERROR_MISSING_ARGS);
	}
	else
	{
		child = params;
		while(child != NULL)
		{
			if(count >= element->params->count)
			{
				printf("Excess of params when call %s\n", function->data->original);
				exit(IKS_ERROR_EXCESS_ARGS);
			}
			else
			{
				type = element->params->list[count];

				if(type != child->returnType)
				{
					printf("Param %d must be %d\n", count + 1, element->params->list[count]);
					exit(IKS_ERROR_WRONG_TYPE_ARGS);
				} 
			}

			count++;
			child = child->child;
		}
	}
}

void checkType(int typeA, int typeB)
{
	int result;
	//printf("check if %d = %d\n", typeA, typeB);

	if(typeA != ISK_STRING && typeB == ISK_STRING)
	{
		printf("invalid conversion from string\n");
		exit(IKS_ERROR_STRING_TO_X);
	}
	if(typeA != ISK_CHAR && typeB == ISK_CHAR)
	{
		printf("invalid conversion from char\n");
		exit(IKS_ERROR_CHAR_TO_X);
	}
	if((typeA == ISK_STRING && typeB != ISK_STRING) || (typeA == ISK_CHAR && typeB != ISK_CHAR))
	{
		printf("variables are incompatible\n");
		exit(IKS_ERROR_WRONG_TYPE);
	}
}

void findFunction(comp_dict_item_t* item)
{
	comp_info_stack_t * element;

	//printf("find function: %s\n", item->original);

	element = stackFind(item->original, 0);

	if(element < 0)
	{
		printf("%s is not declared\n", item->original);
		exit(IKS_ERROR_UNDECLARED);
	}
	else if(element->function != true)
	{
		printf("%s is not a function\n", item->original);
		exit(IKS_ERROR_VARIABLE);
	}
}

void findVariable(comp_tree_t * item, bool vector)
{
	comp_info_stack_t * element;

	//printf("find variable: %s with type: %d\n", item->original, item->type);

	element = stackFind(item->data->original, 0);

	if(element == NULL)
	{
		printf("%s is not declared\n", item->data->original);
		exit(IKS_ERROR_UNDECLARED);
	}
	else if(element->function != false)
	{
		printf("%s is a function\n", item->data->original);
		exit(IKS_ERROR_FUNCTION);
	}
	else
	{
		if(vector && element->vector == false)
		{
			printf("%s variable is not a vector\n", item->data->original);
			exit(IKS_ERROR_VARIABLE);
		}
		else if(vector == false && element->vector == true)
		{
			printf("%s variable is a vector\n", item->data->original);
			exit(IKS_ERROR_VECTOR);
		}

		item->address = element->address;
		item->global = element->global;
		item->params = element->params;
		item->returnType = element->type;
	}
}

void inputTest(comp_dict_item_t * tree)
{
	if(tree->type != SIMBOLO_IDENTIFICADOR)
	{
		printf("input need a variable, but you give %d\n", tree->type);
		exit(IKS_ERROR_WRONG_PAR_INPUT);
	}
}

void outputTest(comp_tree_t * tree)
{
	if(tree->returnType == ISK_CHAR || tree->returnType == ISK_BOOL)
	{
		printf("output must be an arithmetic or string\n");
		exit(IKS_ERROR_WRONG_PAR_OUTPUT);
	}
}

void returnTest(int type)
{
	comp_info_stack_t * function;
	int lastFunction = -1;
	int position = 0;

	function = stackHead(true);

	if(function == NULL)
	{
		printf("return must be associated with a function\n");
		exit(-1);
	}
	else if(function->type != type)
	{
		if(type == ISK_CHAR || type == ISK_STRING || function->type == ISK_CHAR || function->type == ISK_STRING)
		{
			printf("wrong type of return\n");
			exit(IKS_ERROR_WRONG_PAR_RETURN);
		}
	}	
}

void setPartialType(comp_tree_t * father, comp_tree_t * childA, comp_tree_t * childB)
{
	comp_info_stack_t * element;

	switch(father->type)
	{
		case AST_FUNCAO:
		case AST_IDENTIFICADOR:
			element = stackFind(father->data->original, 0);

			if(element == NULL)
			{
				printf("%s is not declared\n", father->data->original);
				exit(IKS_ERROR_UNDECLARED);
			}

			father->returnType = element->type;
			break;
		case AST_VETOR_INDEXADO:
			element = stackFind(childA->data->original, 0);

			if(element == NULL)
			{
				printf("%s is not declared\n", childA->data->original);
				exit(IKS_ERROR_UNDECLARED);
			}

			father->returnType = element->type;
			break;


			break;
		case AST_LITERAL:

			father->returnType = father->data->type;
			break;

		case AST_ARIM_DIVISAO:
		case AST_ARIM_MULTIPLICACAO:
		case AST_ARIM_SUBTRACAO:
		case AST_ARIM_SOMA:

			checkType(ISK_FLOAT, childA->returnType);
			checkType(ISK_FLOAT, childB->returnType);

			if(childA->returnType == ISK_FLOAT || childB->returnType == ISK_FLOAT)
			{
				father->returnType = ISK_FLOAT;
			}
			else
			{
				father->returnType = ISK_INT;
			}
			break;

		case AST_ARIM_INVERSAO:

			checkType(ISK_FLOAT, childA->returnType);

			father->returnType = childA->returnType;
			break;

		case AST_LOGICO_E:
		case AST_LOGICO_OU:

			checkType(ISK_BOOL, childA->returnType);
			checkType(ISK_BOOL, childB->returnType);

			father->returnType = ISK_BOOL;
			break;

		case AST_LOGICO_COMP_DIF:
		case AST_LOGICO_COMP_IGUAL:
		case AST_LOGICO_COMP_LE:
		case AST_LOGICO_COMP_GE:
		case AST_LOGICO_COMP_L:
		case AST_LOGICO_COMP_G:

			checkType(ISK_FLOAT, childA->returnType);
			checkType(ISK_FLOAT, childB->returnType);

			father->returnType = ISK_BOOL;
			break;

		case AST_LOGICO_COMP_NEGACAO:

			checkType(ISK_BOOL, childA->returnType);

			father->returnType = ISK_BOOL;
			break;

		case AST_CHAMADA_DE_FUNCAO:

			element = stackFind(father->child->data->original, 0);

			if(element == NULL)
			{
				printf("%s is not declared\n", father->child->data->original);
				exit(IKS_ERROR_UNDECLARED);
			}

			father->returnType = element->type;
			break;

		case AST_SHIFT_RIGHT:
		case AST_SHIFT_LEFT:

			father->returnType = ISK_INT;
			break;
	}
}

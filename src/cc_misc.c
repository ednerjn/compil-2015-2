#include "cc_misc.h"

int lineNumber = 1;

int countLines(char * input)
{
	char * pointer = NULL;
	int counter = 0;
	
	pointer = strchr(input,'\n');
  	
	while (pointer != NULL)
  	{
    	counter++;
		pointer = strchr(pointer+1,'\n');
	}
	
	return(counter);
}

int getLineNumber (void)
{
  return lineNumber;
}

void addLineNumber(int number)
{
	lineNumber += number;	
}

void yyerror(const char *s)
{
	printf("%d: %s\n", lineNumber, s);
}

void main_init (int argc, char **argv)
{
 	//implemente esta função com rotinas de inicialização, se necessário
	initHash(&symbol_hash);
}

void main_finalize (void)
{
  //implemente esta função com rotinas de inicialização, se necessário
}

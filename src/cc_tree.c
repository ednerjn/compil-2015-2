#include "cc_tree.h"

comp_tree_t * ast;

void addChild(comp_tree_t * father, comp_tree_t * child)
{
	comp_tree_t * target;

	if(father != NULL && child != NULL)
	{
		if(father->child == NULL)
		{
			father->child = child;
		}
		else
		{
			target = father->child;

			while(target->brother != NULL)
			{
				target = target->brother;
			}

			target->brother = child;
		}
	}
}

comp_tree_t * createNode(int type, comp_dict_item_t * data)
{
	comp_tree_t * node;
	
	node = (comp_tree_t *) malloc(sizeof(comp_tree_t));

	if(node != NULL)
	{
		node->data = data;
		node->type = type;
		node->returnType = -1;
		node->brother = NULL;
		node->child = NULL;
	}

	return node;
}

bool isLeaf(comp_tree_t * node)
{
	if(node->child == NULL)
	{
		return true;
	}
	else
	{
		return false;
	}
}

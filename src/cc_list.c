#include "cc_list.h"

comp_stack_t stack;
comp_scope_t * scopeStack;

void stackInit()
{
	stack.size = 0;
}

int stackPush(comp_dict_item_t * element, int type, int size, int address, bool function, bool global)
{
	if (stack.size < STACK_MAX)
	{
		stack.data[stack.size].data = element;
		stack.data[stack.size].type = type;
		stack.data[stack.size].size = size;
		stack.data[stack.size].address = address;
		stack.data[stack.size].function = function;
		stack.data[stack.size].global = global;
		stack.data[stack.size].params = NULL;

		stack.size++;

		return(stack.size - 1);
	}
	else
	{
		printf("Error: stack full\n");
		exit(-1);
	}
}

void stackPop()
{
	if (stack.size == 0)
	{
		printf("Error: stack empty\n");
	}
	else
	{
		stack.size--;
	}
}

comp_info_stack_t * stackFind(char * name, int limit)
{
	int position = stack.size -1;

	while(position >= limit && strcmp(name, stack.data[position].data->original) != 0)
	{
		position --;
	}

	if(position < limit)
	{
		return NULL;
	}
	else
	{
		return &stack.data[position];
	}
}

comp_info_stack_t * stackGet(int position)
{
	if(position < 0 || position >= stack.size)
	{
		return NULL;
	}
	else
	{
		return &stack.data[position];
	}
}

comp_info_stack_t * stackHead(bool function)
{
	int position;

	position = stack.size - 1;

	while(position >= 0 && stack.data[position].function != function)
	{
		position--;
	}

	if(position < 0)
	{
		return NULL;
	}
	else
	{
		return &stack.data[position];
	}
}

void pushScope(void)
{
	comp_scope_t *scope;

	scope = malloc(sizeof(comp_scope_t));

	scope->index = stack.size - 1;
	scope->next = scopeStack;
	scopeStack = scope;

	//printf("change Scope at %d\n", scope->index);
}

void popScope(void)
{
	comp_scope_t * actual;

	int top;
	int stop;

	if(scopeStack == NULL)
	{
		printf("Missing Scope\n");
	}
	else
	{
		actual = scopeStack;
		
		//printf("close Scope at %d\n", actual->index);

		scopeStack = actual->next;

		top = stack.size - 1;
		stop = actual->index;

		while(top > stop)
		{
			stackPop();
			top--;
		}

		free(actual);
	}
}

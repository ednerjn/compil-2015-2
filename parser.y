%{
	#include <stdio.h>	

	#define YYERROR_VERBOSE

%}

%union 
{
	comp_dict_item_t * valor_simbolo_lexico;
	comp_dict_item_t * var;
	comp_dict_item_t * actualFunction;
	comp_tree_t * tree;

	int size;
	int type;
}

%code requires
{
	#include "cc_dict.h"
	#include "cc_list.h"
	#include "cc_tree.h"
	#include "cc_semantic.h"
	#include "cc_code.h"
}

%type <tree> program
%type <tree> pg.body
%type <tree> function
%type <tree> fn.name
%type <tree> fnc.list
%type <tree> fnc.more
%type <tree> fn.block
%type <tree> fn.bk.element
%type <tree> fn.bk.el.more
%type <tree> command
%type <tree> command_block
%type <tree> fn_call
%type <tree> cmb.el.more
%type <tree> cmb.element
%type <tree> if
%type <tree> expression
%type <tree> while
%type <tree> input
%type <tree> output
%type <tree> out.list
%type <tree> attribute
%type <tree> return
%type <tree> id
%type <tree> shift
%type <tree> index
%type <tree> ix.param
%type <tree> ix.pr.more
%type <type> type
%type <var> sid

/* Declaração dos tokens da linguagem */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_STATIC
%token TK_PR_CONST
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING
%token TK_IDENTIFICADOR
%token TOKEN_ERRO

%left TK_OC_OR 
%left TK_OC_AND 
%left TK_OC_GE TK_OC_LE TK_OC_EQ TK_OC_NE
%left '+' '-'
%left '*' '/'
%left '<' '>'
%left '=' '!'
%left TK_OC_SR TK_OC_SL
%right TK_PR_THEN TK_PR_ELSE

%%
/* Regras (e ações) da gramática */

program: pg.body {$$ = createNode(AST_PROGRAMA,NULL); addChild($$,$1); ast = $$; generateCode(ast);};

pg.body:  function pg.body {$$ = $1; addChild($$,$2);}
		| global_var pg.body {$$ = $2;}
		| {$$ = NULL;};

/* Global Variable & Function Declaration */

function: fn.name fn.params fn.block {$$ = $1; addChild($$,$3);};
fn.name:  TK_PR_STATIC type sid {$$ = createNode(AST_FUNCAO, yylval.valor_simbolo_lexico); addFunction($$->data, $2); $$->returnType = $2; pushScope();}
		| type sid {$$ = createNode(AST_FUNCAO, yylval.valor_simbolo_lexico); addFunction(yylval.valor_simbolo_lexico, $1); $$->returnType = $1; pushScope();};
fn.params: '(' fn.element;
fn.element:   ')' 
			| type sid fn.el.more {addParam($1);} 
			| TK_PR_CONST type sid fn.el.more {addParam($2);};
fn.el.more:   ')' 
			| ',' type sid fn.el.more {addParam($2);}
			| ',' TK_PR_CONST type sid fn.el.more {addParam($3);};

fn.block: '{' fn.bk.element {$$ = $2;};
fn.bk.element:	'}' {$$ = NULL; popScope();} 
				| command fn.bk.el.more {$$ = $1; $$ == NULL? $$ = $2 : addChild($$,$2);};
fn.bk.el.more:	'}' {$$ = NULL; popScope();} 
				| semicolon '}' {$$ = NULL; popScope();} 
				| semicolon command fn.bk.el.more {$$ = $2; $$ == NULL? $$ = $3 : addChild($$,$3);};

global_var: gv.name gv.vector semicolon;

gv.name:  type sid {addVariable($2, $1, true);}
		| TK_PR_STATIC type sid {addVariable($3, $2, true);};

gv.vector: | '[' gv.vec.value gv.vec.more;
gv.vec.more:  ']'
			| ',' gv.vec.value gv.vec.more;
gv.vec.value: TK_LIT_INT {addVectorParam(yylval.valor_simbolo_lexico->value.integer);};

/* Commands */

command:  local_var {$$ = NULL;}
		| attribute {$$ = $1;}
		| input {$$ = $1;}
		| output {$$ = $1;}
		| return {$$ = $1;}
		| empty {$$ = NULL;}
		| fn_call {$$ = $1;}
		| shift {$$ = $1;}
		| if {$$ = $1;}
		| while {$$ = $1;}
		| command_block {$$ = $1;};

command_block: '{' cmb.empty cmb.element {$$ = createNode(AST_BLOCO,NULL); addChild($$,$3);};
cmb.empty: {pushScope();};
cmb.element:  '}' {$$ = NULL; popScope();}
			| command cmb.el.more {$$ = $1; $$ == NULL? $$ = $2 : addChild($$,$2);};
cmb.el.more:  '}' {$$ = NULL; popScope();}
			| semicolon '}' {$$ = NULL; popScope();} 
			| semicolon command cmb.el.more {$$ = $2; $$ == NULL? $$ = $3 : addChild($$,$3);};

while: TK_PR_WHILE '(' expression ')' TK_PR_DO command {$$ = createNode(AST_WHILE_DO,NULL); addChild($$,$3); addChild($$,$6);}
		| TK_PR_DO command TK_PR_WHILE '(' expression ')' {$$ = createNode(AST_DO_WHILE,NULL); addChild($$,$2); addChild($$,$5);};

if:   TK_PR_IF '(' expression ')' TK_PR_THEN command {$$ = createNode(AST_IF,NULL); addChild($$,$3); addChild($$,$6);}
	| TK_PR_IF '(' expression ')' TK_PR_THEN command TK_PR_ELSE command {$$ = createNode(AST_IF_ELSE,NULL); addChild($$,$3); addChild($$,$6); addChild($$,$8);};

local_var:    TK_PR_STATIC TK_PR_CONST lv.suffix;
			| TK_PR_STATIC lv.suffix;
			| TK_PR_CONST lv.suffix;
			| lv.suffix;
lv.suffix:    lv.name 
			| lv.name lv.vector
			| lv.name TK_OC_LE sid
			| lv.name TK_OC_LE value;

lv.name:	  type sid {addVariable($2, $1, false);};

lv.vector: 	'[' lv.vec.value lv.vec.more;
lv.vec.more:  ']'
			| ',' lv.vec.value lv.vec.more;
lv.vec.value: TK_LIT_INT {addVectorParam(yylval.valor_simbolo_lexico->value.integer);};


attribute:	id '=' expression {$$ = createNode(AST_ATRIBUICAO,NULL); addChild($$,$1); addChild($$,$3); findVariable($1, false); setPartialType($1, NULL, NULL); checkType($1->returnType, $3->returnType);}
			| index '=' expression {$$ = createNode(AST_ATRIBUICAO, NULL); addChild($$, $1); addChild($$,$3); checkType($1->returnType, $3->returnType	);};

input: TK_PR_INPUT expression TK_OC_GE expression {$$ = createNode(AST_INPUT, NULL); addChild($$, $2); addChild($$, $4); inputTest($2->data);}; 

output: TK_PR_OUTPUT out.list {$$ = createNode(AST_OUTPUT,NULL); addChild($$,$2);};
out.list: expression {$$ = $1; outputTest($1);}
		| expression ',' out.list {$$ = $1; addChild($$,$3); outputTest($1);};

return: TK_PR_RETURN expression {$$ = createNode(AST_RETURN,NULL); addChild($$,$2); returnTest($2->returnType);};

empty: semicolon;

fn_call: id '(' fnc.list  {$$ = createNode(AST_CHAMADA_DE_FUNCAO, NULL); addChild($$, $1); addChild($$, $3); checkParams($1, $3);};
fnc.list: ')' {$$ = NULL;} 
		| expression fnc.more {$$ = $1; $$ == NULL? $$ = $2 : addChild($$, $2);}; 
fnc.more: ')' {$$ = NULL;}
		| ',' expression fnc.more {$$ = $2; $$ == NULL? $$ = $3 : addChild($$, $3);};

shift:	id TK_OC_SR expression {$$ = createNode(AST_SHIFT_RIGHT,NULL); addChild($$,$1); addChild($$,$3); findVariable($1, false); checkType(TK_LIT_INT, $3->returnType);}
		| id TK_OC_SL expression {$$ = createNode(AST_SHIFT_LEFT,NULL); addChild($$,$1); addChild($$,$3); findVariable($1, false); checkType(TK_LIT_INT, $3->returnType);};

/* Expression */

expression:   id {$$ = $1; findVariable($1, false); setPartialType($$, NULL, NULL);}
			| value {$$ = createNode(AST_LITERAL,yylval.valor_simbolo_lexico); setPartialType($$, NULL, NULL);}
			| index {$$ = $1;}
			| expression '+' expression {$$ = createNode(AST_ARIM_SOMA, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression '-' expression {$$ = createNode(AST_ARIM_SUBTRACAO, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression '/' expression {$$ = createNode(AST_ARIM_DIVISAO, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression '*' expression {$$ = createNode(AST_ARIM_MULTIPLICACAO, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression '<' expression {$$ = createNode(AST_LOGICO_COMP_L, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression '>' expression {$$ = createNode(AST_LOGICO_COMP_G, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression '=' expression {$$ = createNode(AST_LOGICO_COMP_L, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression TK_OC_LE expression {$$ = createNode(AST_LOGICO_COMP_LE, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression TK_OC_GE expression {$$ = createNode(AST_LOGICO_COMP_GE, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression TK_OC_EQ expression {$$ = createNode(AST_LOGICO_COMP_IGUAL, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);};
			| expression TK_OC_NE expression {$$ = createNode(AST_LOGICO_COMP_DIF, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression TK_OC_AND expression{$$ = createNode(AST_LOGICO_E, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| expression TK_OC_OR expression {$$ = createNode(AST_LOGICO_OU, NULL); addChild($$, $1); addChild($$, $3); setPartialType($$, $1, $3);}
			| '(' expression ')' {$$ = $2;}
			| '-' expression {$$ = createNode(AST_ARIM_INVERSAO, NULL); addChild($$, $2); setPartialType($$, $2, NULL);}
			| '!' expression {$$ = createNode(AST_LOGICO_COMP_NEGACAO, NULL); addChild($$, $2); setPartialType($$, $2, NULL);}
			| fn_call {$$ = $1; setPartialType($$, NULL, NULL);}; 

index: id '[' ix.param {$$ = createNode(AST_VETOR_INDEXADO, NULL); addChild($$, $1); addChild($$, $3); findVariable($1, true); setPartialType($$, $1, NULL);};
ix.param: expression ix.pr.more {$$ = $1; addChild($1, $2); checkType(TK_LIT_INT, $1->returnType);};
ix.pr.more:   ']' {$$ = NULL;} 
			| ',' expression ix.pr.more {$$ = $2; addChild($2, $3); checkType(TK_LIT_INT, $2->returnType);};

/* Extra Rules */

semicolon: ';';

id: TK_IDENTIFICADOR {$$ = createNode(AST_IDENTIFICADOR,yylval.valor_simbolo_lexico);};
sid: TK_IDENTIFICADOR {$$ = yylval.valor_simbolo_lexico;};
type:	 TK_PR_BOOL {$$ = ISK_BOOL;}
		| TK_PR_CHAR {$$ = ISK_CHAR;}
		| TK_PR_FLOAT {$$ = ISK_FLOAT;}
		| TK_PR_INT {$$ = ISK_INT;}
		| TK_PR_STRING {$$ = ISK_STRING;};
value: TK_LIT_CHAR | TK_LIT_FALSE | TK_LIT_FLOAT | TK_LIT_INT | TK_LIT_STRING | TK_LIT_TRUE; 

%%

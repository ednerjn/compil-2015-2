%{
#include <stdbool.h>
#include "parser.h" //arquivo automaticamente gerado pelo bison
#include "main.h"
#include "cc_dict.h"
#include "cc_misc.h"

int add_symbol(int type);

%}

%x GCOMMENT

ALPHANUM [A-Za-z_0-9]
LETTER [A-Za-z_]
NUMBER [0-9]+


DQUOTE [\"]
SQUOTE [\']

FALSE "false"|"FALSE"
TRUE "true"|"TRUE"

NEW_LINE \n

CHAR	{SQUOTE}(.){SQUOTE}
FLOAT	{NUMBER}("."){NUMBER}
ID		{LETTER}{ALPHANUM}*
INTEGER	{NUMBER}
STRING 	"\""([^\n\"\\]*(\\[.\n])*)*"\""

SCOMMENT	("//")(.)*

%%

{NEW_LINE} {addLineNumber(1);}

%{ // Palavras Reservadas %}

("int")		{return TK_PR_INT;}
"float"		{return TK_PR_FLOAT;}
"bool"		{return TK_PR_BOOL;}
"char"		{return TK_PR_CHAR;}
"string"	{return TK_PR_STRING;}
"if"		{return TK_PR_IF;}
"then"		{return TK_PR_THEN;}
"else"		{return TK_PR_ELSE;}
"while"		{return TK_PR_WHILE;}
"do"		{return TK_PR_DO;}
"input"		{return TK_PR_INPUT;}
"output"	{return TK_PR_OUTPUT;}
"return"	{return TK_PR_RETURN;}
"const"		{return TK_PR_CONST;}
"static"	{return TK_PR_STATIC;}

%{ // Caracteres  Especiais %}

[\,\;\:\(\)\[\]\{\}\+\-\*\/\<\>\=\!\&\$]	{return yytext[0];}

%{ // Operadores Compostos %}

"<="	{return TK_OC_LE;}
">="	{return TK_OC_GE;}
"=>"	{return TK_OC_GE;}
"=="	{return TK_OC_EQ;}
"!="	{return TK_OC_NE;}
"&&"	{return TK_OC_AND;}
"||"	{return TK_OC_OR;}
">>"	{return TK_OC_SR;}
"<<"	{return TK_OC_SL;}

%{ // Identificador %}

{ID}	{return add_symbol(TK_IDENTIFICADOR);}

%{ // Literais %}

{CHAR}		{return add_symbol(TK_LIT_CHAR);}
{FLOAT}		{return add_symbol(TK_LIT_FLOAT);}
{FALSE}		{return TK_LIT_FALSE;}
{INTEGER}	{return add_symbol(TK_LIT_INT);}
{TRUE}		{return TK_LIT_TRUE;}
{STRING}	{return add_symbol(TK_LIT_STRING);}

<INITIAL>
{
	"/*"	BEGIN(GCOMMENT);
}
<GCOMMENT>{
	"*/"	BEGIN(INITIAL);
	[^*\n]+
	"*"
	\n		addLineNumber(1);
}

{SCOMMENT}	{}
" "|\t		{}
.			{return TOKEN_ERRO;}

%%

int add_symbol(int type)
{
	char * value = NULL;
	comp_dict_item_t * item = NULL;
	
	value = (char *) malloc(sizeof(char) * (strlen(yytext) + 1));
	
	if(type == TK_LIT_STRING || type == TK_LIT_CHAR)
	{
		strcpy(value,&yytext[1]);
		value[strlen(yytext)-2] = '\0';
	}
	else
	{
		strcpy(value,yytext);
	}
	
	item = findHash(&symbol_hash, value);

	if(item == NULL)
	{
		item = addHash(&symbol_hash, value);
	}

	if(item != NULL)
	{
		yylval.valor_simbolo_lexico = item;
				
		item->line = getLineNumber();
		item->original = value;
		
		switch(type)
		{
			case TK_LIT_CHAR:
				item->type = SIMBOLO_LITERAL_CHAR;
				item->value.character = value[0];

				break;
			case TK_LIT_FALSE:
				item->type = SIMBOLO_LITERAL_BOOL;
				item->value.boolean = false;

				break;
			case TK_LIT_FLOAT:
				item->type = SIMBOLO_LITERAL_FLOAT;
				item->value.real = atof(value);

				break;
			case TK_LIT_INT:
				item->type = SIMBOLO_LITERAL_INT;
				item->value.integer = atoi(value);

				break;
			case TK_LIT_STRING:
				item->type = SIMBOLO_LITERAL_STRING;
				item->value.string = value;

				break;
			case TK_LIT_TRUE:
				item->type = SIMBOLO_LITERAL_BOOL;
				item->value.boolean = true;
				break;
			case TK_IDENTIFICADOR:
				item->type = SIMBOLO_IDENTIFICADOR;
		}
		
		return type;
	}
	else
	{
		free(value);
		return TOKEN_ERRO;
	}
}

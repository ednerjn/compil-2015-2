#ifndef __LIST_H
#define __LIST_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "cc_dict.h"
#include "main.h"

#define STACK_MAX 64000

typedef struct comp_info_stack
{
	bool function;
	bool global;
	bool vector;
	comp_dict_item_t * data;
	comp_param_t * params;
	int address;
	int size;
	int type;
} comp_info_stack_t;

typedef struct comp_stack 
{
	comp_info_stack_t data[STACK_MAX];
	int	 size;
} comp_stack_t;

typedef struct comp_scope
{
	int index;
	struct comp_scope *next;
} comp_scope_t;

extern comp_stack_t stack;
extern comp_scope_t * scopeStack;

void stackInit(void);

int stackPush(comp_dict_item_t * element, int type, int size, int address, bool function, bool global);

void stackPop(void);

comp_info_stack_t * stackFind(char * name, int limit);

comp_info_stack_t * stackGet(int position);

comp_info_stack_t * stackHead(bool function);

void pushScope(void);

void popScope(void);

#endif


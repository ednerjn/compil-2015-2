#ifndef __TREE_H
#define __TREE_H

#include <stdio.h>
#include <stdbool.h>
#include "cc_ast.h"
#include "cc_dict.h"
#include "cc_gv.h"

typedef struct comp_tree
{
	bool global;
	int address;
	int type;
	int returnType;
	comp_dict_item_t * data;
	comp_param_t * params;
	struct comp_tree * brother;
	struct comp_tree * child;
} comp_tree_t;

extern comp_tree_t * ast;

/**
 * Adiciona um filho no nodo pai
 * @param comp_tree_t father Nodo pai
 * @param comp_tree_t child Nodo filho
**/
void addChild(comp_tree_t * father, comp_tree_t * child);

/**
 * Cria um nodo de uma arvore. 
 * @param int type Tipo do elemento da arvore
 * @param comp_dict_item_t Ponteiro para os dados do nodo
 * @return comp_tree_t * Retorna o ponteiro para o elemento criado
**/
comp_tree_t * createNode(int type, comp_dict_item_t * data);

/**
 * Determina se o elemento informado é uma folha
 * @param comp_tree_t * node Elemento a ser avaliado
 * @return bool Caso seja uma folha retorna true, caso contrário false
**/
bool isLeaf(comp_tree_t * node);

/**
 * Remove o elemento informado no parametro node
 * @param comp_tree_t * node Elemento a ser removido
 * @return Caso a operação se realize com sucesso retorna true, caso contrário false
**/
bool removeNode(comp_tree_t * node);

#endif

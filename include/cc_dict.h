#ifndef __DICT_H
#define __DICT_H
	
#define __USE_GNU
#define _GNU_SOURCE 

#include <stdlib.h>
#include <search.h>
#include <string.h>
#include <stdbool.h>

#define MAX_HASH_TABLE 64000

typedef struct comp_param
{

	int count;
	int list[16];
} comp_param_t;

typedef struct comp_dict_item
{
	char * original;
	int line;
	int type;
	union
	{
		bool boolean;
		char character;
		char * string;
		float real;
		int integer;
	} value;
	
} comp_dict_item_t;

typedef struct hsearch_data comp_dict_t;
	
extern comp_dict_t symbol_hash;

/**
	Adiciona um elemento na tabela hash informada
	@param comp_dict_t table Ponteiro para a tabela hash
	@param string item_key chave do elemento a ser inserido na tabela
	@return comp_dict_item_t devolve o elemento criado ou null em caso de falha
**/
comp_dict_item_t * addHash(comp_dict_t * table, char * item_key);

/**
	Localiza um element dentro da tabela hash
	@param comp_dict_t table Ponteiro para a tabela hash
	@param string item_key chave do elemento a ser encontrado
	@return comp_dict_t devolve o elemento criado ou null em caso de falha
**/
comp_dict_item_t * findHash(comp_dict_t * table, char * item_key);

/**
	Inicializa a tabela hash
	@param comp_dict_t table Tabela a ser inicializada
	@return retorna 0 em caso de sucesso, -1 em caso de falha
**/
int initHash(comp_dict_t * table);

/**
	Remove um elemento na tabela hash informada
	@param comp_dict_t table Ponteiro para a tabela hash
	@param string item_key chave do elemento a ser removido da tabela
	@return integer retorna 0 em caso de sucesso e -1 em caso de falha
**/
int removeHash(comp_dict_t * table, char * item_key);

#endif

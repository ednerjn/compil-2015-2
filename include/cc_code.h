#ifndef __CODE_H
#define __CODE_H

#include <stdlib.h>
#include <stdio.h>
#include "cc_list.h"
#include "cc_dict.h"
#include "cc_tree.h"
#include "cc_semantic.h"

#define TYPE_REGISTER 1
#define TYPE_LABEL 2

#define OPCODE_NOP       0
#define OPCODE_ADD       1
#define OPCODE_SUB       2
#define OPCODE_MULT      3
#define OPCODE_DIV       4
#define OPCODE_ADDI      5
#define OPCODE_SUBI      6
#define OPCODE_RSUBI     7
#define OPCODE_MULTI     8
#define OPCODE_DIVI      9
#define OPCODE_RDIVI    10
#define OPCODE_LSHIFT   11
#define OPCODE_LSHIFTI  12
#define OPCODE_RSHIFT   13
#define OPCODE_RSHIFTI  14
#define OPCODE_AND      15
#define OPCODE_ANDI     16
#define OPCODE_OR       17
#define OPCODE_ORI      18
#define OPCODE_XOR      19
#define OPCODE_XORI     20
#define OPCODE_LOAD     21
#define OPCODE_LOADAI   22
#define OPCODE_LOADAO   23
#define OPCODE_CLOAD    24
#define OPCODE_CLOADAI  25
#define OPCODE_CLOADAO  26
#define OPCODE_LOADI    27
#define OPCODE_STORE    28
#define OPCODE_STOREAI  29
#define OPCODE_STOREAO  30
#define OPCODE_CSTORE   31
#define OPCODE_CSTOREAI 32
#define OPCODE_CSTOREAO 33
#define OPCODE_I2I      34
#define OPCODE_C2C      35
#define OPCODE_C2I      36
#define OPCODE_I2C      37
#define OPCODE_JUMPI    38
#define OPCODE_JUMP     39
#define OPCODE_CBR      40
#define OPCODE_CMP_LT   41
#define OPCODE_CMP_LE   42
#define OPCODE_CMP_EQ   43
#define OPCODE_CMP_GE   44
#define OPCODE_CMP_GT   45
#define OPCODE_CMP_NE   46

typedef struct comp_operation
{
	char * label;
	char * sourceA;
	char * sourceB;
	char * target;
	int type;
	struct comp_operation *back;
	struct comp_operation *next;
} comp_operation_t;

typedef struct comp_instruction
{
	struct comp_instruction *back;
	struct comp_instruction *next;
	comp_operation_t *firstOp;
	comp_operation_t *lastOp;
	
} comp_instruction_t;


typedef struct comp_code_param
{
	char * rr; // Return Register
	char * lt; // Case true
	char * lf; // Cae false
} comp_code_param_t;

/**
 * Gera o código baseado na AST
 * @param tree: AST
**/

void generateCode(comp_tree_t * tree);

#endif

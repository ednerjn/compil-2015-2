#ifndef __SEMANTIC_H
#define __SEMANTIC_H

#include <stdlib.h>
#include <stdio.h>
#include "cc_list.h"
#include "cc_dict.h"

#define IKS_SUCCESS 0
#define IKS_ERROR_UNDECLARED 1
#define IKS_ERROR_DECLARED 2
#define IKS_ERROR_VARIABLE 3
#define IKS_ERROR_VECTOR 4
#define IKS_ERROR_FUNCTION 5
#define IKS_ERROR_WRONG_TYPE 6
#define IKS_ERROR_STRING_TO_X 7
#define IKS_ERROR_CHAR_TO_X 8
#define IKS_ERROR_MISSING_ARGS 9
#define IKS_ERROR_EXCESS_ARGS 10
#define IKS_ERROR_WRONG_TYPE_ARGS 11
#define IKS_ERROR_WRONG_PAR_INPUT 12
#define IKS_ERROR_WRONG_PAR_OUTPUT 13
#define IKS_ERROR_WRONG_PAR_RETURN 14

#define ISK_INT 1
#define ISK_FLOAT 2
#define ISK_CHAR 3
#define ISK_STRING 4
#define ISK_BOOL 5

/**
 * @param item: Elemento a ser adicionado a pilha
 * @param type: Tipo do elemento
**/

void addFunction(comp_dict_item_t* item, int type);

/**
 * Adiciona um parametro na função atual
 * @param type: Tipo da variavel
**/

void addParam(int type);

/**
 * Adiciona uma variável a pilha de variáveis
 * @param item: Elemento a ser adicionado a pilha
 * @param type: Tipo da variável
 * @param global: Define se a variavel é global ou local
 **/

void addVariable(comp_dict_item_t* item, int type, bool global);

void addVectorParam(int size);

/**
 * Verifica se todos os parametros passados na chamada de função estão na quantidade exigida e nos tipos corretos.
 * @param function: Função a ser avaliada
 * @param params: Nodo da arvore que reune os parametros passados para a chamada de função
 **/


void checkParams(comp_tree_t * function, comp_tree_t * params);

/**
 * Determina se as variáveis são compatíveis
 * @param typeA: Tipo da variável alvo
 * @param typeB: Tipo da variável secundária
 **/

void checkType(int typeA, int typeB);

/**
 * Localiza uma função na pilha de funções
 * @param item: Variável a ser encontrada
 **/

void findFunction(comp_dict_item_t* item);

/**
 * Localiza uma variável na pilha
 * @param item: Função a ser encontrada
 * @param vector: Indica se a variavel esperada é um vetor
**/

void findVariable(comp_tree_t * item, bool vector);

/**
 * Verifica se o parâmetro do input é uma variável
 * @param item: Elemento a ser testado
 **/

void inputTest(comp_dict_item_t * item);

/**
 * Verifica se os parâmetros de saída são números ou strings
 * @param item: raiz da árvore de parâmetros
 **/

void outputTest(comp_tree_t * item);

void returnTest(int type);

void setPartialType(comp_tree_t * father, comp_tree_t * childA, comp_tree_t * childB);

#endif
